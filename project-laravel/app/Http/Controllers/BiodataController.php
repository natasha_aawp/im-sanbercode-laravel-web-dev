<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    //
    public function daftar()
    {
        return view('page.biodata');
    }

    public function kirim(Request $request)
    {
        // dd($request->all());
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('page.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
